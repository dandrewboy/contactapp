package com.example.contactsapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends BaseAdapter {
    Activity mActivity;
    List<BaseContact> list;

    public ContactAdapter(Activity mActivity, List<BaseContact> list) {
        this.mActivity = mActivity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public BaseContact getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View oneContactLine;

        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        oneContactLine = inflater.inflate(R.layout.activity_contact_one_line, parent, false);

        TextView tv_name = oneContactLine.findViewById(R.id.tv_nameLable);
        TextView tv_number = oneContactLine.findViewById(R.id.tv_phoneNumberLable);
        ImageView iv_photo = oneContactLine.findViewById(R.id.iv_photoDisplay);

        BaseContact c = this.getItem(position);

        tv_name.setText(c.getName());
        tv_number.setText(c.getPhoneNumber());


        return oneContactLine;

    }
}
