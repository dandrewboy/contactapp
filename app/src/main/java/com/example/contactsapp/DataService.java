package com.example.contactsapp;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class DataService {

   Context context;
   ObjectMapper om = new ObjectMapper();

   public DataService(Context context) {
       this.context = context;
   }

   public void writeList(AddressBook addressBook, String fileName) {

       File path = context.getExternalFilesDir(null);
       File file = new File(path, fileName);

       try{
           om.writerWithDefaultPrettyPrinter().writeValue(file, addressBook);
       } catch (IOException e){
           e.printStackTrace();
       }
   }

   public AddressBook readList(String fileName) {
       AddressBook addressBook = new AddressBook();

       File path = context.getExternalFilesDir(null);
       File file = new File(path, fileName);

       try{
           addressBook = new ObjectMapper().readerFor(AddressBook.class).readValue(file);

       } catch (IOException e) {
           e.printStackTrace();
       }
       return addressBook;
   }
}
