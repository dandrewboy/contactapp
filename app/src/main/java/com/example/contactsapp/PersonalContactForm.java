package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

public class PersonalContactForm extends AppCompatActivity {


    Button b_personalCancel;
    Button b_personalSave, b_image;
    ToggleButton tb_Favorite;

    EditText et_name;
    EditText et_address;
    EditText et_city;
    EditText et_state;
    EditText et_postalCode;
    EditText et_Country;
    EditText et_phoneNumber;
    EditText et_photo;
    EditText et_email;
    EditText et_dateOfBirth;
    EditText et_relationship;

    boolean isFavorite = false;
    String type = "Personal";
    String newName;
    String newAddress;
    String newCity;
    String newState;
    String newPostalCode;
    String newCountry;
    String newPhoneNumber;
    String newPhoto;
    String newEmail;
    Boolean favorite;
    String newDateOfBirth;
    String newRelationship;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_contact);

        b_personalCancel = findViewById(R.id.b_Cancel);
        b_personalSave = findViewById(R.id.b_personalSave);
        b_image = findViewById(R.id.b_image);
        tb_Favorite = findViewById(R.id.tb_Favorite);

        et_name = findViewById(R.id.et_Name);
        et_address = findViewById(R.id.et_Address);
        et_city = findViewById(R.id.et_City);
        et_state = findViewById(R.id.et_State);
        et_postalCode = findViewById(R.id.et_PostalCode);
        et_Country = findViewById(R.id.et_Country);
        et_phoneNumber = findViewById(R.id.et_PhoneNumber);
        et_photo = findViewById(R.id.et_Photo);
        et_email = findViewById(R.id.et_email);
        et_dateOfBirth = findViewById(R.id.et_DateOfBirth);
        et_relationship = findViewById(R.id.et_Relationship);

        Bundle incomeingIntent = getIntent().getExtras();
        if(incomeingIntent != null) {
            et_photo.setText(incomeingIntent.getString("uri"));
        }

        if(tb_Favorite.isChecked() == true) {
            isFavorite = true;
        } else {
            isFavorite = false;
        }

        b_personalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToAddContact = new Intent(v.getContext(), AddContact.class);
                startActivity(goToAddContact);
            }
        });

        b_personalSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newName = et_name.getText().toString();
                newAddress = et_address.getText().toString();
                newCity = et_city.getText().toString();
                newState = et_state.getText().toString();
                newPostalCode = et_postalCode.getText().toString();
                newCountry = et_Country.getText().toString();
                newPhoneNumber = et_phoneNumber.getText().toString();
                newPhoto = et_photo.getText().toString();
                newEmail = et_email.getText().toString();
                favorite = isFavorite;
                newDateOfBirth = et_dateOfBirth.getText().toString();
                newRelationship = et_relationship.getText().toString();

                PersonalContact pc = new PersonalContact(type, newName, newAddress, newCity, newState,
                        newPostalCode, newCountry, newPhoneNumber, newPhoto, newEmail, favorite,
                        newDateOfBirth, newRelationship);

               businessService.getList().addOne(pc);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");

                Intent goToAddContact = new Intent(v.getContext(), AddContact.class);
                startActivity(goToAddContact);
            }
        });

        b_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ImageSelect.class);
                i.putExtra("intent", 1);
                startActivity(i);
            }
        });
    }
}
