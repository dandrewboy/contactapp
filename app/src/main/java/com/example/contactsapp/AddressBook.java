package com.example.contactsapp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class AddressBook {

    //This is the main data storage structure for the entire application.
    //this class demonstrate the use of generics

    public List<BaseContact> theList;

    @JsonIgnore
    public List<BaseContact> searchResults;

    public AddressBook() {
        this.theList = new ArrayList<BaseContact>();
        this.searchResults = new ArrayList<BaseContact>();
    }

    // add one person/business contact to the list
    // The T generic data type allows this function  to handle any class inherited from
    // the BaseContact - person or business
    public <T extends BaseContact> void addOne(T contact) {
        this.theList.add(contact);
    }

    // remove a contact from the main list. return true for success and return false for failure
    public <BaseContact> boolean deleteOne(int i) {
        if (this.theList.contains(theList.get(i))) {
            this.theList.remove(theList.get(i));
            return true;
        }
        return false;

    }


    public List<BaseContact> searchFor(String s) {
        searchResults.clear();
        for (int i = 0; i < theList.size(); i++) {
            if(theList.get(i).name.contains(s) == true) {
                searchResults.add(theList.get(i));
            }
        }
//        if(s.equals(".*\\d+.*")) {
//            int j = Integer.parseInt(s);
//            if(j - 1 < theList.size() && theList.contains(theList.get(j - 1))) {
//                searchResults.add(theList.get(j - 1));
//            }
//        }
        return searchResults;
    }

    public BaseContact searchByNum(Integer i) {
        if( i - 1 < theList.size() && theList.contains(theList.get(i - 1))) {
            return theList.get(i - 1);
        }

        return null;

    }

    public String searchByNumName(Integer i) {
        if( i - 1 < theList.size() && theList.contains(theList.get(i - 1))) {
            return theList.get(i - 1).name;
        }

        return null;

    }


    public List<BaseContact> getTheList() {
        return theList;
    }

    public int findPosition(String s) {
        int position = 0;
        for(int i = 0; i < theList.size(); i++) {
            if(theList.get(i).name.equals(s)) {
                position = i;
                break;
            }
        }
        return position;
    }






    @Override
    public String toString() {
        return "AddressBook [theList=" + theList + "]";
    }


}
