package com.example.contactsapp;

public class PersonalContact extends BaseContact {
    private String dateOfBirth;
    private String relationship;



    public String getDateOfBirth() {
        return dateOfBirth;
    }



    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }



    public String getRelationship() {
        return relationship;
    }



    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }



    public PersonalContact(String type, String name, String streetName, String city, String state, String postalCode, String country,
                           String phoneNumber, String photoName, String email, Boolean favorite, String dateOfBirth,
                           String relationship) {
        super(type, name, streetName, city, state, postalCode, country, phoneNumber, photoName, email, favorite);
        this.dateOfBirth = dateOfBirth;
        this.relationship = relationship;
    }

    //default constructor
//    public PersonalContact() {
//        super("Personal", "PersonName", "StreetName", "CityName", "State", "PostalCode", "Country", "PhoneNumber", "Image", "Email", false);
//        this.dateOfBirth = "dd/mm/yy";
//        this.relationship = "Mother, Brother, Friend, ect.";
//    }

    public PersonalContact() {
        super();

    }


    @Override
    public String toString() {
        return "PersonalContact{" +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", streetName='" + streetName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", photoName='" + photoName + '\'' +
                ", email='" + email + '\'' +
                ", favorite=" + favorite + '\'' +
                "dateOfBirth='" + dateOfBirth + '\'' +
                ", relationship='" + relationship +
                '}';
    }

    @Override
    public int compareTo(BaseContact other) {
        int compareResult = this.name.compareTo(other.name);
        return compareResult;
    }

}
