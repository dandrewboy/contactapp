package com.example.contactsapp;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BaseContact.class, name = "BaseContact"),
        @JsonSubTypes.Type(value = PersonalContact.class, name = "Personal"),
        @JsonSubTypes.Type(value = BusinessContact.class, name = "Business")
})

public abstract class BaseContact implements Comparable<BaseContact> {
    protected String type;
    protected String name;
    protected String streetName;
    protected String city;
    protected String state;
    protected String postalCode;
    protected String country;
    protected String phoneNumber;
    protected String photoName;
    protected String email;
    protected Boolean favorite;



    public BaseContact() {

    }

    @Override
    public String toString() {
        return "BaseContact [type= " + type + "name=" + name + ", streetName=" + streetName + ", city=" + city + ", state=" + state
                + ", postalCode=" + postalCode + ", country=" + country + ", phoneNumber=" + phoneNumber
                + ", photoName=" + photoName + ", email=" + email + ", favorite=" + favorite + "]";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BaseContact(String type, String name, String streetName, String city, String state, String postalCode, String country,
                       String phoneNumber, String photoName, String email, Boolean favorite) {
        super();
        this.type = type;
        this.name = name;
        this.streetName = streetName;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.photoName = photoName;
        this.email = email;
        this.favorite = favorite;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public void callContact() {
        System.out.println("Calling contact");
    }

    public void textContact() {
        System.out.println("texting contact");
    }

    public void navigateToContact() {
        System.out.println("Navigating to contact");
    }

    public void emailContact() {
        System.out.println("Emailing contact");
    }



}
