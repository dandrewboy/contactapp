package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ToggleButton;

public class BusinessContactForm extends AppCompatActivity {


    Button b_businessSave;
    Button b_businessCancel, b_image;
    ToggleButton tb_favorite;

    EditText et_businessName;
    EditText et_address;
    EditText et_city;
    EditText et_state;
    EditText et_postalCode;
    EditText et_Country;
    EditText et_phoneNumber;
    EditText et_photo;
    EditText et_email;
    EditText et_openingTime;
    EditText et_closingTime;
    EditText et_URL;

    CheckBox cb_Monday;
    CheckBox cb_Tuesday;
    CheckBox cb_Wednesday;
    CheckBox cb_Thursday;
    CheckBox cb_Friday;
    CheckBox cb_Saturday;
    CheckBox cb_Sunday;


    boolean isFavorite = false;
    String type = "Business";
    String newName;
    String newAddress;
    String newCity;
    String newState;
    String newPostalCode;
    String newCountry;
    String newPhoneNumber;
    String newPhoto;
    String newEmail;
    Boolean favorite;
    String newOpeningTime;
    String newClosingTime;
    boolean[] newDaysOfWeekOpen;
    String newURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_contact);

        b_businessCancel = findViewById(R.id.b_Cancel);
        b_businessSave = findViewById(R.id.b_businessSave);
        b_image = findViewById(R.id.b_image);
        tb_favorite = findViewById(R.id.tb_Favorite);

        et_businessName = findViewById(R.id.et_BusinessName);
        et_address = findViewById(R.id.et_Address);
        et_city = findViewById(R.id.et_City);
        et_state = findViewById(R.id.et_State);
        et_postalCode = findViewById(R.id.et_PostalCode);
        et_Country = findViewById(R.id.et_Country);
        et_phoneNumber = findViewById(R.id.et_PhoneNumber);
        et_photo = findViewById(R.id.et_Photo);
        et_email = findViewById(R.id.et_Email);
        et_openingTime = findViewById(R.id.et_OpeningTime);
        et_closingTime = findViewById(R.id.et_ClosingTime);
        et_URL = findViewById(R.id.et_URL);

        cb_Monday = findViewById(R.id.cb_Monday);
        cb_Tuesday = findViewById(R.id.cb_Tuesday);
        cb_Wednesday = findViewById(R.id.cb_Wednesday);
        cb_Thursday = findViewById(R.id.cb_Thursday);
        cb_Friday = findViewById(R.id.cb_Friday);
        cb_Saturday = findViewById(R.id.cb_Saturday);
        cb_Sunday = findViewById(R.id.cb_Sunday);

        b_businessCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToAddContact = new Intent(v.getContext(), AddContact.class);
                startActivity(goToAddContact);
            }
        });

        b_businessSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tb_favorite.isChecked() == true) {
                    isFavorite = true;
                } else {
                    isFavorite = false;
                }

                newName = et_businessName.getText().toString();
                newAddress = et_address.getText().toString();
                newCity = et_city.getText().toString();
                newState = et_state.getText().toString();
                newPostalCode = et_postalCode.getText().toString();
                newCountry = et_Country.getText().toString();
                newPhoneNumber = et_phoneNumber.getText().toString();
                newPhoto = et_photo.getText().toString();
                newEmail = et_email.getText().toString();
                favorite = isFavorite;
                newOpeningTime = et_openingTime.getText().toString();
                newClosingTime = et_closingTime.getText().toString();

                newDaysOfWeekOpen = new boolean[7];
                for(int i = 0; i < newDaysOfWeekOpen.length;) {
                    if(cb_Monday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Tuesday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Wednesday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Thursday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Friday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Saturday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                    if(cb_Sunday.isChecked() == true) {
                        newDaysOfWeekOpen[i] = true;
                    } else {
                        newDaysOfWeekOpen[i] = false;
                    }
                    i++;
                }

                newURL = et_URL.getText().toString();

                BusinessContact bc = new BusinessContact(type, newName, newAddress, newCity, newState, newPostalCode,
                        newCountry, newPhoneNumber, newPhoto, newEmail, favorite, newOpeningTime, newClosingTime,
                        newDaysOfWeekOpen, newURL);
                businessService.getList().addOne(bc);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");


                Intent goToAddContact = new Intent(v.getContext(), AddContact.class);
                startActivity(goToAddContact);
            }
        });

        b_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ImageSelect.class);
                i.putExtra("intent", 3);
                startActivity(i);
                }
        });
    }
}
