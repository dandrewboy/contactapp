package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button b_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b_start = findViewById(R.id.b_start);


        b_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataService dataService = new DataService(v.getContext());
                businessService.setList(   dataService.readList("contactsList.txt"));
                Toast.makeText(MainActivity.this, "items=" + businessService.addressBook.theList.size(), Toast.LENGTH_SHORT).show();
                Intent goToMainMenu = new Intent(v.getContext(), MainMenu.class);
                startActivity(goToMainMenu);
            }
        });








    }
}
