package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AddContact extends AppCompatActivity {

    Button b_personal;
    Button b_business;
    Button b_addCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        b_personal = findViewById(R.id.b_personal);
        b_business = findViewById(R.id.b_business);
        b_addCancel = findViewById(R.id.b_Cancel);

        b_personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToPersonalContact = new Intent(v.getContext(), PersonalContactForm.class);
                startActivity(goToPersonalContact);
            }
        });

        b_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToBusinessContact = new Intent(v.getContext(), BusinessContactForm.class);
                startActivity(goToBusinessContact);
            }
        });

        b_addCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToMainMenu = new Intent(v.getContext(), MainMenu.class);
                startActivity(goToMainMenu);
            }
        });
    }
}
