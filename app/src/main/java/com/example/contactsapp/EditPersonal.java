package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

public class EditPersonal extends AppCompatActivity {

    EditText et_editName;
    EditText et_editAddress;
    EditText et_editCity;
    EditText et_editState;
    EditText et_editPostalCode;
    EditText et_editCountry;
    EditText et_editPhoneNumber;
    EditText et_editPhoto;
    EditText et_editEmail;
    EditText et_editDateOfBirth;
    EditText et_editRelationship;

    Button b_editSave;
    Button b_delete;
    Button b_contact, b_image;
    ToggleButton tb_editFavorite;

    boolean isFavorite;
    String type = "Personal";
    String newName;
    String newAddress;
    String newCity;
    String newState;
    String newPostalCode;
    String newCountry;
    String newPhoneNumber;
    String newPhoto;
    String newEmail;
    Boolean favorite;
    String newDateOfBirth;
    String newRelationship;
    int edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personal);

        et_editName = findViewById(R.id.et_editName);
        et_editAddress = findViewById(R.id.et_editAddress);
        et_editCity = findViewById(R.id.et_editCity);
        et_editState = findViewById(R.id.et_editState);
        et_editPostalCode = findViewById(R.id.et_editPostalCode);
        et_editCountry= findViewById(R.id.et_editCountry);
        et_editPhoneNumber = findViewById(R.id.et_editPhoneNumber);
        et_editPhoto = findViewById(R.id.et_editPhoto);
        et_editEmail = findViewById(R.id.et_editEmail);
        et_editDateOfBirth = findViewById(R.id.et_editDateOfBirth);
        et_editRelationship = findViewById(R.id.et_editRelationship);

        b_editSave = findViewById(R.id.b_editSave);
        b_delete = findViewById(R.id.b_delete);
        b_contact = findViewById(R.id.b_contact);
        b_image = findViewById(R.id.b_image);
        tb_editFavorite = findViewById(R.id.tb_editFavorite);

        Bundle incomingMessage = getIntent().getExtras();
        String name = incomingMessage.getString("name");
        String address = incomingMessage.getString("address");
        String city = incomingMessage.getString("city");
        String state = incomingMessage.getString("state");
        String postalCode = incomingMessage.getString("postal code");
        String country = incomingMessage.getString("country");
        final String phoneNumber = incomingMessage.getString("phone number");
        String photo = incomingMessage.getString("uri");
        isFavorite = incomingMessage.getBoolean("favorite");
        final String email = incomingMessage.getString("email");
        String dateOfBirth = incomingMessage.getString("date of birth");
       String relationship = incomingMessage.getString("relationship");
        edit = incomingMessage.getInt("edit");

        et_editName.setText(name);
        et_editAddress.setText(address);
        et_editCity.setText(city);
        et_editState.setText(state);
        et_editPostalCode.setText(postalCode);
        et_editCountry.setText(country);
        et_editPhoneNumber.setText(phoneNumber);
        et_editPhoto.setText(photo);
        et_editEmail.setText(email);
        et_editDateOfBirth.setText(dateOfBirth);
        et_editRelationship.setText(relationship);

        if(isFavorite == true) {
            tb_editFavorite.setChecked(true);
        } else{
            tb_editFavorite.setChecked(false);
        }

        b_editSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newName = et_editName.getText().toString();
                newAddress = et_editAddress.getText().toString();
                newCity = et_editCity.getText().toString();
                newState = et_editState.getText().toString();
                newPostalCode = et_editPostalCode.getText().toString();
                newCountry = et_editCountry.getText().toString();
                newPhoneNumber = et_editPhoneNumber.getText().toString();
                newPhoto = et_editPhoto.getText().toString();
                newEmail = et_editEmail.getText().toString();
                favorite = isFavorite;
                newDateOfBirth = et_editDateOfBirth.getText().toString();
                newRelationship = et_editRelationship.getText().toString();

                businessService.getList().deleteOne(edit);

                PersonalContact pc = new PersonalContact(type, newName, newAddress, newCity, newState,
                        newPostalCode, newCountry, newPhoneNumber, newPhoto, newEmail, favorite,
                        newDateOfBirth, newRelationship);

                businessService.getList().addOne(pc);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");

                Intent i = new Intent(v.getContext(), SearchContact.class);
                startActivity(i);

            }

        });

        b_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessService.getList().deleteOne(edit);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");

                Intent i = new Intent(v.getContext(), SearchContact.class);
                startActivity(i);
            }
        });

        b_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ComunicationPage.class);
                i.putExtra("phoneNumber", phoneNumber);
                i.putExtra("email", email);
                startActivity(i);
            }
        });

        b_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ImageSelect.class);
                i.putExtra("intent", 2);
                i.putExtra("name", et_editName.getText().toString());
                i.putExtra("address", et_editAddress.getText().toString());
                i.putExtra("city", et_editCity.getText().toString());
                i.putExtra("state", et_editState.getText().toString());
                i.putExtra("postal code", et_editPostalCode.getText().toString());
                i.putExtra("country", et_editCountry.getText().toString());
                i.putExtra("phone number", et_editPhoneNumber.getText().toString());
                i.putExtra("email", et_editEmail.getText().toString());
                i.putExtra("favorite", isFavorite);
                i.putExtra("date of birth",et_editDateOfBirth.getText().toString());
                i.putExtra("relationship", et_editRelationship.getText().toString());
                i.putExtra("edit", edit);
                startActivity(i);
            }
        });
    }
}
