package com.example.contactsapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageSelect extends AppCompatActivity {

    Button b_take, b_save, b_load;
    ImageView iv_image;
    String currentPhotoPath;
    String Uri;
    String name;
    String address;
    String city;
    String state;
    String postalCode;
    String country;
    String phoneNumber;
    String email;
    String dateOfBirth;
    String relationship;
    String openingTime;
    String closingTime;
    String url;
    boolean [] daysOpen;
    boolean isFavorite;
    int edit;
    int sourceOfIntent;

    static final int REQUEST_TAKE_PHOTO = 1;
    static final int SELECT_A_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_select);

        b_take = findViewById(R.id.b_take);
        b_save = findViewById(R.id.b_save);
        b_load = findViewById(R.id.b_load);

        iv_image = findViewById(R.id.iv_image);

        Bundle incomeingIntent = getIntent().getExtras();
        if(businessService.getContact(edit) instanceof PersonalContact) {
            sourceOfIntent = incomeingIntent.getInt("intent");
            name = incomeingIntent.getString("name");
            address = incomeingIntent.getString("address");
            city = incomeingIntent.getString("city");
            state = incomeingIntent.getString("state");
            postalCode = incomeingIntent.getString("postal code");
            country = incomeingIntent.getString("country");
            phoneNumber = incomeingIntent.getString("phone number");
            isFavorite = incomeingIntent.getBoolean("favorite");
            email = incomeingIntent.getString("email");
            dateOfBirth = incomeingIntent.getString("date of birth");
            relationship = incomeingIntent.getString("relationship");
            edit = incomeingIntent.getInt("edit");
        }
        if(businessService.getContact(edit) instanceof BusinessContact) {
            sourceOfIntent = incomeingIntent.getInt("intent");
            name = incomeingIntent.getString("name");
            address = incomeingIntent.getString("address");
            city = incomeingIntent.getString("city");
            state = incomeingIntent.getString("state");
            postalCode = incomeingIntent.getString("postal code");
            country = incomeingIntent.getString("country");
            phoneNumber = incomeingIntent.getString("phone number");
            isFavorite = incomeingIntent.getBoolean("favorite");
            email = incomeingIntent.getString("email");
            openingTime = incomeingIntent.getString("opening time");
            closingTime = incomeingIntent.getString("closing time");
            daysOpen = incomeingIntent.getBooleanArray("days open");
            url = incomeingIntent.getString("url");
            edit = incomeingIntent.getInt("edit");
        }

        b_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        b_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, SELECT_A_PHOTO);
            }
        });

        b_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), PersonalContactForm.class);
                Intent j = new Intent(v.getContext(), EditPersonal.class);
                Intent k = new Intent(v.getContext(), BusinessContactForm.class);
                Intent l = new Intent(v.getContext(), EditBusiness.class);

                if(sourceOfIntent == 1) {
                    i.putExtra("name", name);
                    i.putExtra("address", address);
                    i.putExtra("city", city);
                    i.putExtra("state", state);
                    i.putExtra("postal code", postalCode);
                    i.putExtra("country", country);
                    i.putExtra("phone number", phoneNumber);
                    i.putExtra("email", email);
                    i.putExtra("favorite", isFavorite);
                    i.putExtra("date of birth",dateOfBirth);
                    i.putExtra("relationship", relationship);
                    i.putExtra("edit", edit);
                    i.putExtra("uri", Uri);
                    startActivity(i);

                } else if(sourceOfIntent == 2) {
                    j.putExtra("name", name);
                    j.putExtra("address", address);
                    j.putExtra("city", city);
                    j.putExtra("state", state);
                    j.putExtra("postal code", postalCode);
                    j.putExtra("country", country);
                    j.putExtra("phone number", phoneNumber);
                    j.putExtra("email", email);
                    j.putExtra("favorite", isFavorite);
                    j.putExtra("date of birth",dateOfBirth);
                    j.putExtra("relationship", relationship);
                    j.putExtra("edit", edit);
                    j.putExtra("uri", Uri);
                    startActivity(j);

                } else if(sourceOfIntent == 3) {
                    k.putExtra("name", name);
                    k.putExtra("address", address);
                    k.putExtra("city", city);
                    k.putExtra("state", state);
                    k.putExtra("postal code", postalCode);
                    k.putExtra("country", country);
                    k.putExtra("phone number", phoneNumber);
                    k.putExtra("email", email);
                    k.putExtra("favorite", isFavorite);
                    k.putExtra("opening time", openingTime);
                    k.putExtra("closing time", closingTime );
                    k.putExtra("days of the week", daysOpen);
                    k.putExtra("url", url);
                    k.putExtra("edit", edit);
                    k.putExtra("uri", Uri);
                    startActivity(k);

                } else if(sourceOfIntent == 4) {
                    l.putExtra("name", name);
                    l.putExtra("address", address);
                    l.putExtra("city", city);
                    l.putExtra("state", state);
                    l.putExtra("postal code", postalCode);
                    l.putExtra("country", country);
                    l.putExtra("phone number", phoneNumber);
                    l.putExtra("email", email);
                    l.putExtra("favorite", isFavorite);
                    l.putExtra("opening time", openingTime);
                    l.putExtra("closing time", closingTime );
                    l.putExtra("days of the week", daysOpen);
                    l.putExtra("url", url);
                    l.putExtra("edit", edit);
                    l.putExtra("uri", Uri);
                    startActivity(l);

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            // set image into the image view. using Glide to automatically scale the image and take less memory.
            Glide.with(this).load(currentPhotoPath).into(iv_image);
            // show file name in the text view
            Uri = currentPhotoPath;

            //uriList.add(Uri.fromFile(new File(currentPhotoPath)));
        }
        if (requestCode == SELECT_A_PHOTO && resultCode == RESULT_OK) {
            Uri selectedPhoto = data.getData();
            // set image into the image view. using Glide to automatically scale the image and take less memory.
            Glide.with(this).load(selectedPhoto).into(iv_image);
            // show file name in the text view
            Uri = selectedPhoto.toString();

            //uriList.add(selectedPhoto);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this, "Could not create a file.", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.contactsapp.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
}
