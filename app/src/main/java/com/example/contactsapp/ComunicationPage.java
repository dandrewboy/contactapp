package com.example.contactsapp;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class ComunicationPage extends AppCompatActivity {
    private static final int PERMISSION_TO_CALL = 1 ;
    Button b_call, b_text, b_email;

    String sentPhoneNumber;
    String sentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comunication_page);

        b_call = findViewById(R.id.b_call);
        b_text = findViewById(R.id.b_text);
        b_email = findViewById(R.id.b_email);

        Bundle incomingMessage = getIntent().getExtras();
        sentPhoneNumber = incomingMessage.getString("phoneNumber");
        sentEmail = incomingMessage.getString("email");

        b_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callPhoneNumber(sentPhoneNumber);
            }
        });

        b_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEmail(sentEmail);
            }
        });

        b_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                composeMmsMessage(sentPhoneNumber);

            }
        });


    }

    public void callPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE)
                != PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ComunicationPage.this, new String[] {Manifest.permission.CALL_PHONE}, PERMISSION_TO_CALL);
        } else {

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int [] grantResults) {

        switch (requestCode) {
            case PERMISSION_TO_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    callPhoneNumber(sentPhoneNumber);
                } else {
                    Toast.makeText(this, "Cannot make a call without your permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public void openEmail(String sentEmail) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, sentEmail);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void composeMmsMessage(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}
