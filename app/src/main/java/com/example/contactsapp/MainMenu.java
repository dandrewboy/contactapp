package com.example.contactsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenu extends AppCompatActivity {

    Button b_addContact;
    Button b_searchContact;
    Button b_close;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        b_addContact = findViewById(R.id.b_addContact);
        b_searchContact = findViewById(R.id.b_searchContact);
        b_close = findViewById(R.id.b_close);







        b_addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToAddContact = new Intent(v.getContext(), AddContact.class);
                startActivity(goToAddContact);
            }
        });

        b_searchContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToSearchContact = new Intent(v.getContext(), SearchContact.class);
                startActivity(goToSearchContact);

            }
        });

        b_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenu.this.finish();

            }
        });
    }
}
