package com.example.contactsapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class EditBusiness extends AppCompatActivity {

    EditText et_editName;
    EditText et_editAddress;
    EditText et_editCity;
    EditText et_editState;
    EditText et_editPostalCode;
    EditText et_editCountry;
    EditText et_editPhoneNumber;
    EditText et_editPhoto;
    EditText et_editEmail;
    EditText et_editOpeningTime;
    EditText et_editClosingTime;
    EditText et_editURL;


    CheckBox cb_editMonday;
    CheckBox cb_editTuesday;
    CheckBox cb_editWednesday;
    CheckBox cb_editThursday;
    CheckBox cb_editFriday;
    CheckBox cb_editSaturday;
    CheckBox cb_editSunday;


    Button b_editSave;
    Button b_delete, b_image, b_contact, b_url;
    ToggleButton tb_editFavorite;

    boolean isFavorite;
    String type = "Business";
    String newName;
    String newAddress;
    String newCity;
    String newState;
    String newPostalCode;
    String newCountry;
    String newPhoneNumber;
    String newPhoto;
    String newEmail;
    Boolean favorite;
    String newOpening;
    String newClosing;
    boolean [] newDaysOpen;
    String newUrl;
    int edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business);

        et_editName = findViewById(R.id.et_editName);
        et_editAddress = findViewById(R.id.et_editAddress);
        et_editCity = findViewById(R.id.et_editCity);
        et_editState = findViewById(R.id.et_editState);
        et_editPostalCode = findViewById(R.id.et_editPostalCode);
        et_editCountry = findViewById(R.id.et_editCountry);
        et_editPhoneNumber = findViewById(R.id.et_editPhoneNumber);
        et_editPhoto = findViewById(R.id.et_editPhoto);
        et_editEmail = findViewById(R.id.et_editEmail);
        et_editOpeningTime = findViewById(R.id.et_editOpeningTime);
        et_editClosingTime = findViewById(R.id.et_editClosingTime);
        et_editURL = findViewById(R.id.et_editURL);

        cb_editMonday = findViewById(R.id.cb_editMonday);
        cb_editTuesday = findViewById(R.id.cb_editTuesday);
        cb_editWednesday = findViewById(R.id.cb_editWednesday);
        cb_editThursday = findViewById(R.id.cb_editThursday);
        cb_editFriday = findViewById(R.id.cb_editFriday);
        cb_editSaturday = findViewById(R.id.cb_editSaturday);
        cb_editSunday = findViewById(R.id.cb_editSunday);

        b_editSave = findViewById(R.id.b_editSave);
        b_delete = findViewById(R.id.b_delete);
        b_image = findViewById(R.id.b_image);
        b_contact = findViewById(R.id.b_contact);
        b_url = findViewById(R.id.b_url);
        tb_editFavorite = findViewById(R.id.tb_editFavorite);

        Bundle incomingMessage = getIntent().getExtras();
            String name = incomingMessage.getString("name");
            String address = incomingMessage.getString("address");
            String city = incomingMessage.getString("city");
            String state = incomingMessage.getString("state");
            String postalCode = incomingMessage.getString("postal code");
            String country = incomingMessage.getString("country");
            final String phoneNumber = incomingMessage.getString("phone number");
            String photo = incomingMessage.getString("uri");
            isFavorite = incomingMessage.getBoolean("favorite");
            final String email = incomingMessage.getString("email");
            String openingTime = incomingMessage.getString("opening");
            String closingTime = incomingMessage.getString("closing");
            final boolean[] daysOpen = incomingMessage.getBooleanArray("daysOpen");
            final String url = incomingMessage.getString("URL");
            edit = incomingMessage.getInt("edit");


            et_editName.setText(name);
            et_editAddress.setText(address);
            et_editCity.setText(city);
            et_editState.setText(state);
            et_editPostalCode.setText(postalCode);
            et_editCountry.setText(country);
            et_editPhoneNumber.setText(phoneNumber);
            et_editPhoto.setText(photo);
            et_editEmail.setText(email);
            et_editOpeningTime.setText(openingTime);
            et_editClosingTime.setText(closingTime);
            newDaysOpen = daysOpen;
            et_editURL.setText(url);

            if (isFavorite == true) {
                tb_editFavorite.setChecked(true);
            } else {
                tb_editFavorite.setChecked(false);
            }

            for (int i = 0; i < newDaysOpen.length; ) {
                if (daysOpen[i] == true) {
                    cb_editMonday.setChecked(true);
                } else {
                    cb_editMonday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editTuesday.setChecked(true);
                } else {
                    cb_editTuesday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editWednesday.setChecked(true);
                } else {
                    cb_editWednesday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editThursday.setChecked(true);
                } else {
                    cb_editThursday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editFriday.setChecked(true);
                } else {
                    cb_editFriday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editSaturday.setChecked(true);
                } else {
                    cb_editSaturday.setChecked(false);
                }
                i++;
                if (daysOpen[i] == true) {
                    cb_editSunday.setChecked(true);
                } else {
                    cb_editSunday.setChecked(false);
                }
                i++;
            }



        b_editSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newName = et_editName.getText().toString();
                newAddress = et_editAddress.getText().toString();
                newCity = et_editCity.getText().toString();
                newState = et_editState.getText().toString();
                newPostalCode = et_editPostalCode.getText().toString();
                newCountry = et_editCountry.getText().toString();
                newPhoneNumber = et_editPhoneNumber.getText().toString();
                newPhoto = et_editPhoto.getText().toString();
                newEmail = et_editEmail.getText().toString();
                favorite = isFavorite;
                newOpening = et_editOpeningTime.getText().toString();
                newClosing = et_editClosingTime.getText().toString();
                newUrl = et_editURL.getText().toString();

                for(int i = 0; i < newDaysOpen.length;) {
                    if(cb_editMonday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editTuesday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editWednesday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editThursday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editFriday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editSaturday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                    if(cb_editSunday.isChecked() == true) {
                        newDaysOpen[i] = true;
                    } else {
                        newDaysOpen[i] = false;
                    }
                    i++;
                }

                businessService.getList().deleteOne(edit);

                BusinessContact bc = new BusinessContact(type, newName, newAddress, newCity, newState, newPostalCode,
                        newCountry, newPhoneNumber, newPhoto, newEmail, favorite, newOpening, newClosing,
                        newDaysOpen, newUrl);

                businessService.getList().addOne(bc);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");

                Intent i = new Intent(v.getContext(), SearchContact.class);
                startActivity(i);

    }
});

        b_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessService.getList().deleteOne(edit);
                DataService dataService = new DataService(v.getContext());
                dataService.writeList(businessService.getList(), "contactsList.txt");

                Intent i = new Intent(v.getContext(), SearchContact.class);
                startActivity(i);
            }
        });

        b_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ImageSelect.class);
                i.putExtra("intent", 4);
                i.putExtra("name", et_editName.getText().toString());
                i.putExtra("address", et_editAddress.getText().toString());
                i.putExtra("city", et_editCity.getText().toString());
                i.putExtra("state", et_editState.getText().toString());
                i.putExtra("postal code", et_editPostalCode.getText().toString());
                i.putExtra("country", et_editCountry.getText().toString());
                i.putExtra("phone number", et_editPhoneNumber.getText().toString());
                i.putExtra("email", et_editEmail.getText().toString());
                i.putExtra("favorite", isFavorite);
                i.putExtra("date of birth",et_editOpeningTime.getText().toString());
                i.putExtra("relationship", et_editClosingTime.getText().toString());
                i.putExtra("days of the week", daysOpen);
                i.putExtra("url", et_editURL.getText().toString());
                i.putExtra("edit", edit);
                startActivity(i);
            }
        });

        b_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), ComunicationPage.class);
                i.putExtra("phoneNumber", phoneNumber);
                i.putExtra("email", email);
                startActivity(i);
            }
        });

        b_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              openWebPage(url);
            }
        });

    }

    public void openWebPage(String url) {
        if(!url.startsWith("http://") || !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
