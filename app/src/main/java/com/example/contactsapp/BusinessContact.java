package com.example.contactsapp;

import java.util.Arrays;

public class BusinessContact extends BaseContact {
    private String opening;
    private String closing;
    private boolean[] daysOfTheWeekOpen;
    private String url;

    public String getOpening() {
        return opening;
    }


    public void setOpening(String opening) {
        this.opening = opening;
    }


    public String getClosing() {
        return closing;
    }


    public void setClosing(String closing) {
        this.closing = closing;
    }


    public boolean[] getDaysOfTheWeekOpen() {
        return daysOfTheWeekOpen;
    }

    public boolean getDaysOfTheWeekOpen(int i) {
        return daysOfTheWeekOpen[i];
    }


    public void setDaysOfTheWeekOpen(boolean[] daysOfTheWeekOpen) {
        this.daysOfTheWeekOpen = daysOfTheWeekOpen;
    }


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public BusinessContact(String type, String name, String streetName, String city, String state, String postalCode, String country,
                           String phoneNumber, String photoName, String email, Boolean favorite, String opening, String closing,
                           boolean[] daysOfTheWeekOpen, String url) {
        super(type, name, streetName, city, state, postalCode, country, phoneNumber, photoName, email, favorite);
        this.opening = opening;
        this.closing = closing;
        this.daysOfTheWeekOpen = daysOfTheWeekOpen;
        this.url = url;
    }

    // default constructor
//    public BusinessContact() {
//        super("Business", "BusinessName", "StreetName", "CityName", "State", "PostalCode", "Country", "PhoneNumber", "Image", "Email", false);
//        this.opening = "12:00 A.M.";
//        this.closing = "12:00 P.M";
//        this.daysOfTheWeekOpen = new boolean[] {false, true, true, true, true, true, false};
//        this.url = "htpp://www.notawebsite.com";
//    }

    public BusinessContact() {
        super();
    }


    @Override
    public String toString() {
        return "BusinessContact{" +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", streetName='" + streetName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", photoName='" + photoName + '\'' +
                ", email='" + email + '\'' +
                ", favorite=" + favorite + '\'' +
                "opening='" + opening + '\'' +
                ", closing='" + closing + '\'' +
                ", daysOfTheWeekOpen=" + Arrays.toString(daysOfTheWeekOpen) +
                ", url='" + url +
                '}';
    }



        public void openURL () {
            System.out.println("Opening web page to " + this.getUrl());
        }


        @Override
        public int compareTo (BaseContact other){
            int compareResult = this.name.compareTo(other.name);
        return compareResult;
    }

        }


