package com.example.contactsapp;

import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

public class SearchContact extends AppCompatActivity {


    List<BaseContact> l;

    Button b_favorites;
    Button b_cancel;
    Button b_search;

    EditText et_searchBar;
    ListView lv_searchList;

    ContactAdapter adapter;
    AddressBook addressBook;

    int edit;

    public void editContact(int position) {

        Intent i = new Intent(getApplicationContext(), EditPersonal.class);
        Intent j = new Intent(getApplicationContext(), EditBusiness.class);
        edit = position;

        if(businessService.getContact(position) instanceof PersonalContact) {
            PersonalContact pc = (PersonalContact) businessService.getContact(position);
            i.putExtra("name", pc.getName());
            i.putExtra("address", pc.getStreetName());
            i.putExtra("city", pc.getCity());
            i.putExtra("state", pc.getState());
            i.putExtra("postal code", pc.getPostalCode());
            i.putExtra("country", pc.getCountry());
            i.putExtra("phone number", pc.getPhoneNumber());
            i.putExtra("uri", pc.getPhotoName());
            i.putExtra("email", pc.getEmail());
            i.putExtra("favorite", pc.getFavorite());
            i.putExtra("date of birth",pc.getDateOfBirth());
            i.putExtra("relationship", pc.getRelationship());
            i.putExtra("edit", edit);
            startActivity(i);

        }
        if(businessService.getContact(position) instanceof BusinessContact) {
            BusinessContact bc = (BusinessContact) businessService.getContact(position);
            j.putExtra("name", bc.getName());
            j.putExtra("address", bc.getStreetName());
            j.putExtra("city", bc.getCity());
            j.putExtra("state", bc.getState());
            j.putExtra("postal code", bc.getPostalCode());
            j.putExtra("country", bc.getCountry());
            j.putExtra("phone number", bc.getPhoneNumber());
            j.putExtra("uri", bc.getPhotoName());
            j.putExtra("email", bc.getEmail());
            j.putExtra("favorite", bc.getFavorite());
            j.putExtra("opening", bc.getOpening());
            j.putExtra("closing", bc.getClosing());
            j.putExtra("daysOpen", bc.getDaysOfTheWeekOpen());
            j.putExtra("URL", bc.getUrl());
            j.putExtra("edit", edit);
            startActivity(j);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);

        b_favorites = findViewById(R.id.b_favorites);
        b_cancel = findViewById(R.id.b_cancel);
        b_search = findViewById(R.id.b_search);
        et_searchBar = findViewById(R.id.et_searchbar);
        lv_searchList = findViewById(R.id.lv_searchList);

        addressBook = businessService.getList();

        b_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String search = et_searchBar.getText().toString();
                l = businessService.getList().searchFor(search);

                adapter = new ContactAdapter(SearchContact.this, l);

                lv_searchList.setAdapter(adapter);

            }
        });

        b_favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToFavorites = new Intent(v.getContext(), Favorites.class);
                startActivity(goToFavorites);
            }
        });

        b_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToMainMenu = new Intent(v.getContext(), MainMenu.class);
                startActivity(goToMainMenu);
            }
        });

        lv_searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // l = clicked contact.


                // what is the position of l in the global address

                String name = l.get(position).getName();
                int listPosition = businessService.getList().findPosition(name);
                editContact(listPosition);


            }
        });
    }
}
